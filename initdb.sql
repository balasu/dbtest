USE `titainic`;

DROP TABLE IF EXISTS `titainic`;

CREATE TABLE `titainic` (
    `uuid` char(36) NULL,
    `survived` varchar(6) not null, 
	`pclass` varchar(6),
	`pname` varchar(255) unique,
	`sex` varchar(6),
	`age` varchar(6) not null,
	`sibsp` varchar(6),
	`parch` varchar(6),
	`fare` varchar(255)
);


--
-- Dumping data for table `titainic`
--
DELIMITER ;;
CREATE TRIGGER before_insert_titainic
BEFORE INSERT ON titainic
FOR EACH ROW
BEGIN
  IF new.uuid IS NULL THEN
    SET new.uuid = uuid();
  END IF;
END
;;
DELIMITER ;
